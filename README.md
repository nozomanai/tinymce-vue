# 封装基于vue的tinymce富文本，可以直接使用，快速开发

# 安装&启动

 - 安装 npm install
 - 启动：npm serve


- 官网的tinymce使用需要apiKey，cdn是国外的，富文本首次加载会很慢
网上的本地化，会有各种问题。

- 自己把本地化和cnd的代码结合，达到了一个比较完美的程度。在vue项目使用基本没什么问题。


上传图片需要配置：
src/components/tinymce/tinymce.vue：

在imagesUploadHandler这个图片上传触发函数配置图片上传路径，请求方法，或者添加请求头token

图片上传如果失败，默认会转换为base64存储


![输入图片说明](https://images.gitee.com/uploads/images/2021/1027/114722_8d2e3931_8841086.png "屏幕截图.png")
